package com.vhiep.domain.model

data class Note(var id: Int = 0, var title: String, var content: String)