package com.vhiep.domain.usecase

import com.vhiep.domain.model.Note
import com.vhiep.domain.repository.NoteRepository

class EditNoteUseCase(private val noteRepository: NoteRepository) {
    suspend operator fun invoke(note: Note) = noteRepository.editNote(note)
}