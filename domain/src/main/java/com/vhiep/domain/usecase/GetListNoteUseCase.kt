package com.vhiep.domain.usecase

import com.vhiep.domain.model.Note
import com.vhiep.domain.repository.NoteRepository

class GetListNoteUseCase(private val noteRepository: NoteRepository) {
    suspend operator fun invoke(): List<Note> = noteRepository.getListNote()
}