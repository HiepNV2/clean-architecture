package com.vhiep.domain.repository

import com.vhiep.domain.model.Note

interface NoteRepository {
    suspend fun createNote(note: Note)

    suspend fun editNote(note: Note)

    suspend fun deleteNote(note: Note)

    suspend fun getListNote(): List<Note>
}