package com.vhiep.cleanarchitecture.presentation.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.vhiep.cleanarchitecture.databinding.ItemNoteBinding
import com.vhiep.domain.model.Note

class NoteAdapter(
    private val onClickEdit: (Note) -> Unit,
    private val onClickDelete: (Note) -> Unit
) : ListAdapter<Note, NoteAdapter.NoteViewHolder>(NoteDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoteViewHolder {
        return NoteViewHolder(
            ItemNoteBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ),
            onClickEdit,
            onClickDelete
        )
    }

    override fun onBindViewHolder(holder: NoteViewHolder, position: Int) {
        holder.bind(currentList[position])
    }

    class NoteViewHolder(
        private val binding: ItemNoteBinding,
        private val onClickEdit: (Note) -> Unit,
        private val onClickDelete: (Note) -> Unit
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(note: Note) {
            binding.apply {
                tvTitle.text = note.title
                tvContent.text = note.content
                ivEdit.setOnClickListener {
                    onClickEdit(note)
                }
                ivDelete.setOnClickListener {
                    onClickDelete(note)
                }
            }
        }
    }

    class NoteDiffCallback : DiffUtil.ItemCallback<Note>() {
        override fun areItemsTheSame(oldItem: Note, newItem: Note): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: Note, newItem: Note): Boolean {
            return oldItem == newItem
        }
    }
}