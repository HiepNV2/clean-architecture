package com.vhiep.cleanarchitecture.presentation.home

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.vhiep.domain.model.Note
import com.vhiep.domain.usecase.CreateNoteUseCase
import com.vhiep.domain.usecase.DeleteNoteUseCase
import com.vhiep.domain.usecase.EditNoteUseCase
import com.vhiep.domain.usecase.GetListNoteUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val getListNoteUseCase: GetListNoteUseCase,
    private val createNoteUseCase: CreateNoteUseCase,
    private val editNoteUseCase: EditNoteUseCase,
    private val deleteNoteUseCase: DeleteNoteUseCase
) : ViewModel() {

    private val _listNote = MutableLiveData<List<Note>>()
    val listNote: MutableLiveData<List<Note>> = _listNote

    init {
        getListNote()
    }

    fun createNote(title: String, content: String) {
        viewModelScope.launch(Dispatchers.IO) {
            createNoteUseCase.invoke(Note(title = title, content = content))
            getListNote()
        }
    }

    private fun getListNote() {
        viewModelScope.launch(Dispatchers.IO) {
            getListNoteUseCase.invoke().let {
                _listNote.postValue(it)
            }
        }
    }

    fun editNote(note: Note) {
        viewModelScope.launch(Dispatchers.IO) {
            editNoteUseCase.invoke(note)
            getListNote()
        }
    }

    fun deleteNote(note: Note) {
        viewModelScope.launch(Dispatchers.IO) {
            deleteNoteUseCase.invoke(note)
            getListNote()
        }
    }
}