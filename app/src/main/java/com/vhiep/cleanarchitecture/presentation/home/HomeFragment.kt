package com.vhiep.cleanarchitecture.presentation.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.LinearLayout
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.vhiep.cleanarchitecture.databinding.FragmentHomeBinding
import com.vhiep.domain.model.Note
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeFragment : Fragment() {
    private lateinit var binding: FragmentHomeBinding
    private val viewModel: HomeViewModel by viewModels()
    private val adapter by lazy {
        NoteAdapter(onClickEdit = {
            showDialogEditNote(it)
        }, onClickDelete = {
            viewModel.deleteNote(it)
        })
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.apply {
            rvNote.adapter = adapter

            btnAdd.setOnClickListener {
                showDialogAddNote()
            }

            viewModel.listNote.observe(viewLifecycleOwner) {
                it?.let {
                    adapter.submitList(it)
                }
            }
        }
    }

    private fun showDialogAddNote() {
        context?.let {
            AlertDialog.Builder(it).apply {
                setTitle("Create note")
                val layout = LinearLayout(context)
                layout.orientation = LinearLayout.VERTICAL
                val edtTitle = EditText(context).apply {
                    setSingleLine()
                    hint = "Title"
                }
                layout.addView(edtTitle)
                val edtContent = EditText(context).apply {
                    hint = "Content"
                }
                layout.addView(edtContent)
                layout.setPadding(50, 50, 50, 50)
                setView(layout)
                setPositiveButton("Create") { dialog, _ ->
                    viewModel.createNote(edtTitle.text.toString(), edtContent.text.toString())
                    dialog.dismiss()
                }
                setNegativeButton("Cancel") { dialog, _ ->
                    dialog.dismiss()
                }
                setCancelable(false)
                show()
            }
        }
    }

    private fun showDialogEditNote(note: Note) {
        context?.let {
            AlertDialog.Builder(it).apply {
                setTitle("Create note")
                val layout = LinearLayout(context)
                layout.orientation = LinearLayout.VERTICAL
                val edtTitle = EditText(context).apply {
                    setSingleLine()
                    hint = "Title"
                    setText(note.title)
                }
                layout.addView(edtTitle)
                val edtContent = EditText(context).apply {
                    hint = "Content"
                    setText(note.content)
                }
                layout.addView(edtContent)
                layout.setPadding(50, 50, 50, 50)
                setView(layout)
                setPositiveButton("Update") { dialog, _ ->
                    viewModel.editNote(
                        Note(
                            note.id,
                            edtTitle.text.toString(),
                            edtContent.text.toString()
                        )
                    )
                    dialog.dismiss()
                }
                setNegativeButton("Cancel") { dialog, _ ->
                    dialog.dismiss()
                }
                setCancelable(false)
                show()
            }
        }
    }
}