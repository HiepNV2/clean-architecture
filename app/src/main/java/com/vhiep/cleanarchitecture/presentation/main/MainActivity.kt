package com.vhiep.cleanarchitecture.presentation.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.vhiep.cleanarchitecture.R
import com.vhiep.cleanarchitecture.presentation.home.HomeFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportFragmentManager.beginTransaction()
            .add(R.id.frameContainer, HomeFragment(), "HomeFragment").commit()
    }
}