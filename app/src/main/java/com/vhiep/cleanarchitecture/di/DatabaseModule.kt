package com.vhiep.cleanarchitecture.di

import android.content.Context
import androidx.room.Room
import com.vhiep.data.db.NoteDAO
import com.vhiep.data.db.NoteDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class DatabaseModule {

    @Singleton
    @Provides
    fun provideNoteDatabase(@ApplicationContext context: Context): NoteDatabase {
        return Room.databaseBuilder(context, NoteDatabase::class.java, "NoteDB")
            .fallbackToDestructiveMigration().build()
    }

    @Singleton
    @Provides
    fun provideNoteDao(db: NoteDatabase): NoteDAO {
        return db.getNoteDao()
    }
}