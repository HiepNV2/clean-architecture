package com.vhiep.cleanarchitecture.di

import com.vhiep.domain.repository.NoteRepository
import com.vhiep.domain.usecase.CreateNoteUseCase
import com.vhiep.domain.usecase.DeleteNoteUseCase
import com.vhiep.domain.usecase.EditNoteUseCase
import com.vhiep.domain.usecase.GetListNoteUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class UseCaseModule {

    @Singleton
    @Provides
    fun provideGetListNoteUseCase(noteRepository: NoteRepository) =
        GetListNoteUseCase(noteRepository)

    @Singleton
    @Provides
    fun provideCreateNoteUseCase(noteRepository: NoteRepository) = CreateNoteUseCase(noteRepository)

    @Singleton
    @Provides
    fun provideEditNoteUseCase(noteRepository: NoteRepository) = EditNoteUseCase(noteRepository)

    @Singleton
    @Provides
    fun provideDeleteNoteUseCase(noteRepository: NoteRepository) = DeleteNoteUseCase(noteRepository)
}