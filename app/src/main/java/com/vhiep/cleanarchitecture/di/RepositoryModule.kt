package com.vhiep.cleanarchitecture.di

import com.vhiep.data.dataSource.LocalDataSource
import com.vhiep.data.db.NoteEntity
import com.vhiep.data.mapper.EntityMapper
import com.vhiep.data.repository.NoteRepositoryImpl
import com.vhiep.domain.model.Note
import com.vhiep.domain.repository.NoteRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class RepositoryModule {

    @Singleton
    @Provides
    fun provideNoteRepository(
        localDataSource: LocalDataSource,
        entityMapper: EntityMapper<NoteEntity, Note>
    ): NoteRepository = NoteRepositoryImpl(localDataSource, entityMapper)
}