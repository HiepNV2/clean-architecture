package com.vhiep.cleanarchitecture.di

import com.vhiep.data.dataSource.LocalDataSource
import com.vhiep.data.dataSource.LocalDataSourceImpl
import com.vhiep.data.db.NoteDAO
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class DataSourceModule {

    @Singleton
    @Provides
    fun provideLocalDataSource(noteDAO: NoteDAO): LocalDataSource = LocalDataSourceImpl(noteDAO)
}