package com.vhiep.cleanarchitecture.di

import com.vhiep.data.db.NoteEntity
import com.vhiep.data.mapper.EntityMapper
import com.vhiep.data.mapper.NoteMapper
import com.vhiep.domain.model.Note
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class MapperModule {

    @Singleton
    @Provides
    fun provideNoteMapper(): EntityMapper<NoteEntity, Note> = NoteMapper()
}