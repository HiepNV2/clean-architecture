package com.vhiep.cleanarchitecture

import android.app.Application
import dagger.hilt.android.HiltAndroidApp
import javax.inject.Inject

@HiltAndroidApp
class CleanArchitectureApp @Inject constructor() : Application() {
}