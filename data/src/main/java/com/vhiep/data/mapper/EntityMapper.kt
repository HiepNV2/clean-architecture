package com.vhiep.data.mapper

interface EntityMapper<Entity, UIModel> {

    fun mapFromEntity(entity: Entity): UIModel

    fun mapToEntity(uiModel: UIModel): Entity
}