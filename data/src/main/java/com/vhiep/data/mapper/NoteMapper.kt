package com.vhiep.data.mapper

import com.vhiep.data.db.NoteEntity
import com.vhiep.domain.model.Note

class NoteMapper : EntityMapper<NoteEntity, Note> {
    override fun mapFromEntity(entity: NoteEntity): Note {
        return Note(entity.id, entity.title, entity.content)
    }

    override fun mapToEntity(uiModel: Note): NoteEntity {
        return NoteEntity(uiModel.id, uiModel.title, uiModel.content)
    }
}