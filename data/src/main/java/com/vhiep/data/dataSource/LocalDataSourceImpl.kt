package com.vhiep.data.dataSource

import com.vhiep.data.db.NoteDAO
import com.vhiep.data.db.NoteEntity

class LocalDataSourceImpl(private val noteDAO: NoteDAO) : LocalDataSource {
    override fun getListNote(): List<NoteEntity> = noteDAO.getListNotes()

    override fun createNote(note: NoteEntity) = noteDAO.addNote(note)

    override fun editNote(note: NoteEntity) = noteDAO.updateNote(note)

    override fun deleteNote(note: NoteEntity) = noteDAO.deleteNote(note)
}