package com.vhiep.data.dataSource

import com.vhiep.data.db.NoteEntity

interface LocalDataSource {
    fun getListNote(): List<NoteEntity>

    fun createNote(note: NoteEntity)

    fun editNote(note: NoteEntity)

    fun deleteNote(note: NoteEntity)
}