package com.vhiep.data.repository

import com.vhiep.data.dataSource.LocalDataSource
import com.vhiep.data.db.NoteEntity
import com.vhiep.data.mapper.EntityMapper
import com.vhiep.domain.model.Note
import com.vhiep.domain.repository.NoteRepository

class NoteRepositoryImpl(
    private val localDataSource: LocalDataSource,
    private val noteMapper: EntityMapper<NoteEntity, Note>
) : NoteRepository {
    override suspend fun getListNote(): List<Note> =
        localDataSource.getListNote().map { noteMapper.mapFromEntity(it) }

    override suspend fun createNote(note: Note) =
        localDataSource.createNote(noteMapper.mapToEntity(note))

    override suspend fun editNote(note: Note) =
        localDataSource.editNote(noteMapper.mapToEntity(note))

    override suspend fun deleteNote(note: Note) =
        localDataSource.deleteNote(noteMapper.mapToEntity(note))
}