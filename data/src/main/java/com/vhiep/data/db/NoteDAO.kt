package com.vhiep.data.db

import androidx.room.*

@Dao
interface NoteDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addNote(note: NoteEntity)

    @Query("SELECT * FROM Note")
    fun getListNotes(): List<NoteEntity>

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateNote(note: NoteEntity)

    @Delete
    fun deleteNote(note: NoteEntity)
}